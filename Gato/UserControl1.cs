﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gato
{
    public partial class UserControl1 : UserControl
    {
        bool turn = true;
        int turn_count = 0;
        String esganador = "";

        public UserControl1()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, EventArgs e, Button boton)
        {
            if (turn)
            {
                boton.Text = "X";
            }
            else
            {
                boton.Text = "O";
                turn = !turn;
                boton.Enabled = false;
            }


        }


        private void ganador()
        {
            bool ganador = false;
            if ((btn1.Text == btn2.Text) && (btn2.Text == btn3.Text) && (!btn1.Enabled))
            {
                ganador = true;

            }
            if ((btn4.Text == btn5.Text) && (btn5.Text == btn6.Text) && (!btn4.Enabled))
            {
                ganador = true;

            }
            if ((btn7.Text == btn8.Text) && (btn8.Text == btn9.Text) && (!btn7.Enabled))
            {
                ganador = true;

            }
            if (ganador)
            {

                if (turn)
                {
                    esganador = "O ";
                }
                else
                {
                    esganador = "X";
                    MessageBox.Show(esganador + "GANÓ");
               
                }
            }

        }
    }
}