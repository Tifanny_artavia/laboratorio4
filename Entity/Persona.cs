﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public abstract class Persona
    {
        public string Cedula { get; set; }
        public string Nombre { get; set; }
        public Persona()
        {
        }

        public Persona(string cedula, string nombre)
        {
            Cedula = cedula;
            Nombre = nombre;
        }

        public override string ToString()
        {
            return Nombre + "" + Cedula + "" ;
        }
    }
}
