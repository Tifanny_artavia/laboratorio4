﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Usuario : Persona
    {
        public string Ganador { get; set; }
        public Usuario() : base()
        {

        }

        public Usuario(string ganador, string cedula, string nombre)
            : base(cedula, nombre)
        {

            Ganador = ganador;

        }

    }
}
